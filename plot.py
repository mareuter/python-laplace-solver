# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 10:59:28 2014

@author: 2zr
"""
import h5py
import sys

try:
    filename = sys.argv[1]
except IndexError:
    filename = "output.h5"

h5 = h5py.File(filename)
temperature = h5["/temperature"].value
x = h5["/grid/x"].value
y = h5["/grid/y"].value
sim = h5["/simulation"]
num_iter = sim.attrs["num_iterations"]
max_delta = sim.attrs["maximum_delta"]
h5.close()

title = r'$N_{iter}$ = %1.e with $\Delta T_{max}$ = %.2e' % (num_iter, max_delta)

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt

fig = plt.figure()
"""
# Make a 3D plot
ax = plt.subplot(211, projection='3d')
# Need to reshape for 3D use
x1 = x.reshape(x.size, 1)
y1 = y.reshape(1, y.size)
surf = ax.plot_surface(x1, y1, temperature, rstride=1, cstride=1, alpha=0.2, 
                       cmap=cm.jet)
cset = ax.contourf(x, y, temperature, zdir='z', offset=0, cmap=cm.jet)
#cset = ax.contourf(x, y, temperature, zdir='y', offset=0, cmap=cm.jet)
ax.set_zlabel("T (K)")
fig.colorbar(surf, ax=ax)
print title
"""
# Make a 2D contour plot
#ax1 = plt.subplot(212)
ax1 = plt.subplot(111)
plt.title(title)
contour = ax1.contourf(x, y, temperature, 20, cmap=cm.jet)
cb = fig.colorbar(contour, ax=ax1)
cb.set_label("T (K)")

plt.show()
