def run(config):
    import numpy as np

    GRID_SIZE = config.grid_size
    TOTAL_ROWS = TOTAL_COLS = GRID_SIZE + 2
    NUMBER_ITERATIONS = config.niter
    TRACE_ITER = NUMBER_ITERATIONS / 10

    t_curr = np.zeros((TOTAL_ROWS, TOTAL_COLS), dtype=np.float32)

    # Temperature BCS
    t_band = (config.temperature * np.arange(TOTAL_ROWS, dtype=np.float32)) / GRID_SIZE

    # Set the bottom and right edge BCS
    t_curr[-1, :] = t_band
    t_curr[:, -1] = t_band

    delta_t = 0.0

    for i in xrange(NUMBER_ITERATIONS):
        t_next = 0.25 * (t_curr[:-2, 1:-1] + t_curr[2:, 1:-1] +
                         t_curr[1:-1, :-2] + t_curr[1:-1, 2:])
        delta_t = np.max(np.fabs(t_next - t_curr[1:-1, 1:-1]))
        t_curr[1:-1, 1:-1] = t_next

        if i % TRACE_ITER == 0:
            print "--------------- Iteration "+str(i)+" ----------------"
            print "DeltaT = ", delta_t
            print t_curr[-2, -10:]

        #print t_curr

    # Write a HDF5 file
    import h5_writer
    h5 = h5_writer.H5Writer(config.file_tag, "Heat diffusion")
    grid_axis = np.arange(TOTAL_ROWS, dtype=np.float32)
    h5.write_data(t_curr, grid_axis)
    h5.write_info(NUMBER_ITERATIONS, delta_t)
    h5.close()

    # Write a XDMF file
    import xdmf_writer
    xdmf = xdmf_writer.XdmfWriter(config.file_tag)
    xdmf.write_file(t_curr, grid_axis, h5.filename)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--niter", type=int, default=10,
                        help="Specify the number of iterations.")
    parser.add_argument("-s", "--grid-size", type=int, default=1000,
                        help="Specify the grid size.")
    parser.add_argument("-t", "--temperature", type=float, default=100.0,
                        help="Specify the maximum corner temperature.")
    parser.add_argument("-f", "--file-tag", type=str, default="output",
                        help="Specify a file tag for the output.")
    parser.add_argument("-p", "--profile", action="store_true",
                        help="Flag to turn on profiling.")

    args = parser.parse_args()

    if not args.profile:
        run(args)
    else:
        import profile
        profiler = profile.Profile()
        profiler.runcall(run, args)
        profiler.dump_stats("laplace.prof")
