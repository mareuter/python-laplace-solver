def run(config):
    import numpy as np
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    LPROC = size - 1

    # Message tags
    DOWN = 100
    UP = 101

    import parallel_utils as pu
    RANK_ROWS = pu.block_size(rank, size, config.grid_size) + 2
    RANK_COLS = config.grid_size + 2

    TRACE_ITER = config.niter / 10

    t_curr = np.zeros((RANK_ROWS, RANK_COLS), dtype=np.float32)

    # Temperature BCS
    # Set the right edge BCS
    bounds = pu.partition(rank, size, config.grid_size)
    v = np.arange(bounds[0], bounds[1] + 2, dtype=np.float32)
    t_band_rt = (config.temperature * v) / config.grid_size
    t_curr[:, -1] = t_band_rt

    # Set the bottom edge BCS only in the last process
    if rank == LPROC:
        t_band_bot = (config.temperature * np.arange(RANK_COLS, dtype=np.float32)) / config.grid_size
        t_curr[-1, :] = t_band_bot

    delta_t = np.zeros(1, dtype=np.float32)

    for i in xrange(config.niter):
        t_next = 0.25 * (t_curr[:-2, 1:-1] + t_curr[2:, 1:-1] +
                         t_curr[1:-1, :-2] + t_curr[1:-1, 2:])

        if rank < LPROC:
            comm.Send(t_next[-2, 1:-1], dest=rank+1, tag=DOWN)

        if rank != 0:
            comm.Send(t_next[1, 1:-1], dest=rank-1, tag=UP)

        if rank != 0:
            comm.Recv(t_next[0, 1:-1], source=MPI.ANY_SOURCE, tag=DOWN)

        if rank != LPROC:
            comm.Recv(t_next[-1, 1:-1], source=MPI.ANY_SOURCE, tag=UP)

        delta_t_proc = np.max(np.fabs(t_next - t_curr[1:-1, 1:-1]))
        t_curr[1:-1, 1:-1] = t_next

        comm.Reduce(delta_t_proc, delta_t, op=MPI.MAX, root=0)

        if i % TRACE_ITER == 0 and rank == LPROC:
            print "--------------- Iteration "+str(i)+" ----------------"
            #print "Delta T = ", delta_t
            print t_curr[-2, -10:]

        comm.barrier()

    if rank == 0:
        # First process only needs to strip last row
        t_curr = t_curr[:-1, :]
    elif rank == LPROC:
        # Last process only needs to strip first row
        t_curr = t_curr[1:, :]
    else:
        # All other processes strip both first and last row
        t_curr = t_curr[1:-1, :]

    # Make array of the expected block sizes
    counts = [pu.block_size(i, size, config.grid_size) for i in xrange(size)]
    # Need to make first and last block size one row larger
    counts[0] += 1
    counts[-1] += 1

    # Collect all the grids onto a final one.
    t_final = None
    TOTAL_ROWS = config.grid_size + 2
    if rank == 0:
        t_final = np.empty((TOTAL_ROWS, TOTAL_ROWS), dtype=np.float32)

    rowtype = MPI.FLOAT.Create_contiguous(TOTAL_ROWS).Commit()
    comm.Gatherv(t_curr, [t_final, (counts, None), rowtype], 0)
    rowtype.Free()

    if rank == 0:
        # Write a HDF5 file
        import h5_writer
        h5 = h5_writer.H5Writer(config.file_tag, "Heat diffusion")
        grid_axis = np.arange(TOTAL_ROWS, dtype=np.float32)
        h5.write_data(t_final, grid_axis)
        h5.write_info(config.niter, delta_t[0])
        h5.close()

        # Write a XDMF file
        import xdmf_writer
        xdmf = xdmf_writer.XdmfWriter(config.file_tag)
        xdmf.write_file(t_final, grid_axis, h5.filename)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--niter", type=int, default=10,
                        help="Specify the number of iterations.")
    parser.add_argument("-s", "--grid-size", type=int, default=1000,
                        help="Specify the grid size.")
    parser.add_argument("-t", "--temperature", type=float, default=100.0,
                        help="Specify the maximum corner temperature.")
    parser.add_argument("-f", "--file-tag", type=str, default="output",
                        help="Specify a file tag for the output.")

    args = parser.parse_args()

    run(args)

