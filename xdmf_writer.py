# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 20:37:04 2014

@author: 2zr
"""

class XdmfWriter(object):
    
    FILE_EXT = ".xmf"
    DOCTYPE = '<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>'    
    
    def __init__(self, file_tag):
        self.filename = file_tag +self.FILE_EXT
        
    def write_file(self, grid_values, grid_axis, binary_file):
        from lxml import etree
        grid_dims = " ".join([str(x) for x in grid_values.shape])
        root = etree.Element("Xdmf", attrib = {"Version": "2.0"})
        domain = etree.SubElement(root, "Domain")
        grid = etree.SubElement(domain, "Grid", attrib = {"Name" : "solver_grid"})
        topo = etree.SubElement(grid, "Topology", 
                                attrib = {"TopologyType" : "2DRectMesh", 
                                          "Dimensions" : grid_dims})
        geom = etree.SubElement(grid, "Geometry", attrib = {"GeometryType" : "VXVY"})
        data_x = etree.SubElement(geom, "DataItem", 
                                  attrib = {"Name" : "X", 
                                            "Dimensions" : str(grid_axis.size), 
                                            "Precision" : "8", "Format" : "HDF"})
        data_x.text = binary_file + ":/grid/x"
        data_y = etree.SubElement(geom, "DataItem", 
                                  attrib = {"Name" : "Y", 
                                            "Dimensions" : str(grid_axis.size), 
                                            "Precision" : "8", "Format" : "HDF"})
        data_y.text = binary_file + ":/grid/y"
        attribute = etree.SubElement(grid, "Attribute", 
                                     attrib = {"Center" : "Node", 
                                               "Name" : "Temperature"})
        data_grid = etree.SubElement(attribute, "DataItem", 
                                     attrib = {"Dimensions" : grid_dims, 
                                               "Precision" : "8", "Format" : "HDF"})
        data_grid.text = binary_file + ":/temperature"
        xdmf_file = open(self.filename, 'w')
        xdmf_file.write(etree.tostring(root, pretty_print=True, xml_declaration=True, 
                                       doctype=self.DOCTYPE, encoding="utf-8"))
        xdmf_file.close()