# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 20:24:40 2014

@author: 2zr
"""
import h5py

class H5Writer(object):
    
    FILE_EXT = ".h5"    
    
    def __init__(self, file_tag, title=""):
        self.filename = file_tag + self.FILE_EXT
        self.title = title 
        self.h5file = h5py.File(self.filename, 'w')

    def write_data(self, grid_values, grid_axis):
        """
        This function writes the grid values and axis to the HDF file.
        """
        self.h5file.create_dataset("temperature", data=grid_values)
        grid = self.h5file.create_group("grid")
        grid.create_dataset("x", data=grid_axis)
        grid.create_dataset("y", data=grid_axis)

    def write_info(self, num_iter, max_delta):
        """
        This function writes some simulation information to the HDF file.
        """
        sim = self.h5file.create_group("simulation")
        sim.attrs.create("num_iterations", num_iter)
        sim.attrs.create("maximum_delta", max_delta)
        
    def close(self):
        """
        This function closes the HDF file.
        """
        self.h5file.close()