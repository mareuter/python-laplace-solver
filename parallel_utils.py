def partition(rank, size, N): 
    """
    Calculates the partition (array) boundaries based on the rank of the 
    processor, the total number of processors and the size of the partition.

    Parameters
    ----------
    rank : int
        The rank of a given processor.
    size : int
        The total number of processors.
    N : int
        The total partition (array) size to subdivide.

    Returns
    -------
    partition : tuple
        Tuple of two ints containing the lower and upper bounds of the
        partition. When used in standard python range type call, the upper
        bound is exclusive.
    """
    n = N//size + ((N % size) > rank) 
    s = rank * (N//size) 
    if (N % size) > rank: 
        s += rank 
    else: 
        s += N % size 
    return (s, s+n)

def block_size(rank, size, N):
    """
    Calculate the block size from a given N block size based on the current
    rank of the processor and the total number of processors.

    Parameters
    ----------
    rank : int
        The rank of a given processor.
    size : int
        The total number of processors.
    N : int
        The total block size to subdivide.

    Returns
    -------
    block_size : int
        The size for the requested block.
    """
    bounds = partition(rank, size, N)
    return bounds[1] - bounds[0]
